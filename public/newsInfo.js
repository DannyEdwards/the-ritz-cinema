/* 
* JAVASCRIPT DATABANK
* NEWSINFO.JS
*
* Author: Daniel JM Edwards
*/

//initialising the arrays
var Ntitle = new Array();
var Nyear = new Array();
var Nbody = new Array();

//information for all the news items
Ntitle[1] = "July - News Item 1";
Nyear[1] = "2005";
Nbody[1] = "Presentation of cheques by Michele I’Anson, Secretary to the Ritz Volunteer Management Committee to Mrs Eleanor Hooper, from the Joan Maynard Appeal Committee and Mrs Flo Sowerby, Chair of the Thirsk Branch of Yorkshire Cancer Research. <br /> The event was chosen to mark the 10th anniversary of the cinema being re-opened by a team of volunteers. The hit film ‘Calendar Girls’, based in Yorkshire, was screened exactly a decade after the doors re-opened on 4th March 1995. This is the film that tells the true story of the ladies from the Rylstone Women’s Institute and their decision to raise funds for cancer charities after the death of the one of their husbands. <br /> Members of the Ritz Volunteer Management Committee felt that it would be a fitting tribute to donate the proceeds to cancer appeals. Cheques for ₤100 each were presented to Mrs Eleanor Hooper, representing the Joan Maynard Appeal which is creating a palliative care unit at the Lambert Hospital in Thirsk, and to Mrs Flo Sowerby of the Thirsk branch of Yorkshire Cancer Research. <br /> Both Mrs Sowerby and Mrs Hooper were very moved that the cinema had thought of their causes. “Cancer is very much in the media at present and effects so many families. It is nice that the supporters of the Ritz are so generous,” said Mrs Sowerby.";

Ntitle[2] = "May - News Item 2";
Nyear[2] = "2010";
Nbody[2] = "Details 2 lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum.";

Ntitle[3] = "August - News Item 3";
Nyear[3] = "2010";
Nbody[3] = "Details 3 lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum.";