//informaiton for the films showing now and next week:
//NOW SHOWING (NS)
var NStitle = "Twilight Eclipse";
var NSweekBegining = "20th August";
var NSimage = "twilightEclipse.jpg";
var NStrailerURL = "";
var NSrunningTime = "2hr 3min";
var NScert = "12A";
var NSblurb = "The most eagerly awaited film of the summer sees Bella (Kirsten Stewart) and Edward (Robert Pattinson) back again as the exciting story of vampires and werewolves continues.";

//SHOWING NEXT (SN)
var SNtitle = "Knight And Day";
var SNweekBegining = "27th August";
var SNimage = "knightAndDay.jpg";
var SNtrailerURL = "";
var SNrunningTime = "1hr 50min";
var SNcert = "12A";
var SNblurb = "June Havens finds her everyday life tangled with that of a secret agent who has realized he isn't supposed to survive his latest mission...";

//informaiton for the films coming soon (soonest first):
//initialising the arrays
var Ftitle = new Array();
var FweekBegining = new Array();
var Fimage = new Array();
var FtrailerURL = new Array();
var FrunningTime = new Array();
var Fcert = new Array();
var Fblurb = new Array();

//FILM 1 (F1)
Ftitle[1] = "Inception";
FweekBegining[1] = "3rd September";
Fimage[1] = "inception.jpeg";
FtrailerURL[1] = "lkj";
FrunningTime[1] = "2hr 28min";
Fcert[1] = "12A";
Fblurb[1] = "In a world where technology exists to enter the human mind through dream invasion, a highly skilled thief is given a final chance at redemption which involves executing his toughest job till date, Inception.";

//FILM 2 (F2)
Ftitle[2] = "The Sorcerer's Apprentice";
FweekBegining[2] = "10th September";
Fimage[2] = "sorcerersApprentice.jpg";
FtrailerURL[2] = "";
FrunningTime[2] = "1hr 51min";
Fcert[2] = "PG";
Fblurb[2] = "Master sorcerer Balthazar Blake recruits a seemingly everyday guy in his mission to defend New York City from his arch-nemesis, Maxim Horvath.";

//FILM 3 (F3)
Ftitle[3] = "Salt";
FweekBegining[3] = "17th September";
Fimage[3] = "salt.jpg";
FtrailerURL[3] = "";
FrunningTime[3] = "1hr 50min";
Fcert[3] = "12A";
Fblurb[3] = "A CIA agent goes on the run after a defector accuses her of being a Russian spy.";

//FILM 4 (F4)
Ftitle[4] = "The Illusionist";
FweekBegining[4] = "24th September";
Fimage[4] = "theIllusionist.jpg";
FtrailerURL[4] = "";
FrunningTime[4] = "1hr 30min";
Fcert[4] = "12A";
Fblurb[4] = "Details the story of a dying breed of stage entertainer whose thunder is being stolen by emerging rock stars. Forced to accept increasingly obscure assignments in fringe theatres, garden parties and bars, he meets a young fan who changes his life forever.";

//other cinema information:
//PRICES
var adultTicket = "5";
var under15Ticket = "3.50";
var mondayAndMatineeTicket = "3.50";

//SHOW TIMES
var eveningPerformance = "7:30pm";
var matineePerformance = "2:30pm";
var cinemaDoorsOpen = "30";

//DATA
var lastUpdated = "24/07/10";