/* 
* JAVASCRIPT FUNCTIONS
* JAVASCRIPT.JS
*
* Author: Daniel JM Edwards
*/

//this function enables smooth scrolling of the body.
function scrollTo(location){
	var location = location;
	
	if($.browser.opera){
		$('html').animate({scrollTop: $('#'+location).offset().top}, 700);
	}
	else {
		$('html,body').animate({scrollTop: $('#'+location).offset().top}, 700);
	}
}

///jQuery functions
$(document).ready(function(){

	//this function animates the sliding news tabs
	$(".slideOpen").click(function(){
		$(this).children(".content").slideToggle("fast");
	});
	
	//this function animates a showing feature
	$("button").click(function () {
		$("#content").show("fast");
	});
	
	//this function animates the scrollable panel
	$(function() {
		// initialize scrollable panel.
		$(".scrollable").scrollable();
	});

});