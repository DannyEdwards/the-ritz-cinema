<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<meta name="description" content="Ritz Cinema Thirsk - Official Website" />
	<meta name="keywords" content="Ritz Cinema, Ritz Cinema Thirsk, Cinema, Thirsk, Sowerby, Yorkshire, Westgate, Film, Films, Movie, Movies, Volunteer, Herriot" />
	
	<link rel="stylesheet" href="siteStyle.css"/>
	<link rel="shortcut icon" href="" />
	
	<script type="text/javascript" src="jQuery-v1.4.2.js"></script>
	<script type="text/javascript" src="jQuery.tools.js"></script>
	<script type="text/javascript" src="javaScript.js"></script>
	
	<title>Ritz Cinema ~ Administration</title>
</head>

<body>
	<!--HEADER-->
	<div class="header">
		<!--this contains the header graphics-->
		<div class="wrapper">
			<a href="http://twitter.com/RitzCinema" target="_blank">
				<div class="twitter" ></div>
			</a>
			<a href="http://www.facebook.com/group.php?gid=7769358615" target="_blank">
				<div class="facebook" ></div>
			</a>
			<div class="home" onClick="location.href='index.php';" ></div>

			<!--main navigation links-->
			<a href="admin.php" class="currentNavLink">Add Film</a> <h2>~</h2> 
			<a href="comingSoon.html" class="navLink">Add News Item</a> <h2>~</h2>
			<a href="comingSoon.html" class="navLink">Adjust Cinema Attributes</a>
		</div>
	</div>

	<div class="wrapper">
		<!--MAIN-->
		<div class="main">
			<form>
				<label>Title:</label><input type="text" name="title" /><br />
				<label>Week Beginning:</label><input type="text" name="weekBeginning" /><br />
				<label>Running Time:</label><input type="text" name="runningTimeMins" /><br />
				<label>Certificate:</label>
				<select name="cert">
					<option value="U">U</option>
					<option value="PG">PG</option>
					<option value="12A" selected>12A</option>
					<option value="15">15</option>
					<option value="18">18</option>
				</select>
				<br />
				<label>Poster:</label><input type="file" name="image" /><br />
				<label>Trailer URL:</label><input type="text" name="trailerUrl" /><br />
				<label>Blurb:</label><textarea name="blurb"></textarea><br />
				<label></label><input type="submit" value="Submit" /><input type="reset" value="Clear" />
			</form>
		</div>
		
		<!--FOOTER-->
		<div class="footer">
			<a href="index.php">Go Back to Main Site</a>
			<br /><br />
			<div class="disclaimer">
				Site designed and developed by <a href="http://www.igneosaur.webuda.com">Dan Edwards</a> &copy; 2010.
			</div>
		</div>
	</div>
</body>
</html>